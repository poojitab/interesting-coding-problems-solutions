package com.poojita;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Main m = new Main();
        int ip[] = {2,7,11,15};
        int op[] = m.twoSum(ip, 9);
        if(op!=null)
        {
            System.out.println(op[0]+" "+op[1]);
        }
    }

    public int[] twoSum(int[] numbers, int target) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int res[]= new int[2];
        for(int i =0;i<numbers.length;i++)
        {
            if(!hm.containsKey(numbers[i]))
                hm.put(numbers[i], i);

        }
        int index1 =0;
        int index2 = 0;
        for(int i =0; i<numbers.length;i++)
        {
            if(hm.containsKey(target-numbers[i])&& hm.get(target-numbers[i])!=i)
            {
                index1 = i+1;
                index2 = hm.get(target-numbers[i])+1;
                if(index1 > index2)
                {
                    int temp = index1;
                    index1 = index2;
                    index2 = temp;
                }
                res[0]= index1;
                res[1] = index2;

            }
        }

        return res;

    }


}

package com.poojita;

public class Main {

    public static void main(String[] args) {
	// Find Second Max
        Main m = new Main();
        int arr[] = {1,89,56,40,90,100,46,2};
        int res[] = m.minmax(arr);
        for(int j=0;j<res.length;j++)
        {
            System.out.println(res[j]);
        }
    }
    public int[] minmax(int[] arr)
    {
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        for(int i =0;i<arr.length;i++)
        {
            if(arr[i] > max1)
            {
                max2= max1;
                max1 = arr[i];

            }
            else if(arr[i] > max2 && arr[i] != max1)
            {
                max2 = arr[i];
            }


        }

        int arr2[] = {max1,max2};
        return arr2;
    }
}

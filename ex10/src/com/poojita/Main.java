package com.poojita;

public class Main {

    public static void main(String[] args) {
	
        String ret = solution("2-4A0r7-4k", 4);
        String[] abc = {"36 36 30","47 8 60","96 96 96"};
        String[] trips = {"10 2 5", "12 4 4", "6 2 2"};
        String[] ret1 = getTriangleType(abc);
      
        maximumCupcakes(trips);
        int res = countPalindrome("daata");
        System.out.println(res);


    }

            public static String solution(String S, int K) {
                // write your code in Java SE 8

                int count =0;
                StringBuilder finalstr = new StringBuilder(S.replace("-", "").toUpperCase());
                int n = finalstr.length();
                for(int i=n-1;i>=0;i--)
                {
                    count++;
                    if((i+1)%K==0)
                    {
                            if(i > 0) {
                                finalstr.insert(i, '-');
                                count = 0;
                                System.out.println(finalstr);
                            }
                    }




                }
                return finalstr.toString();
            }

    public static String[] getTriangleType(String[] abc) {
        int strlength = abc.length;

        String[] parts = null;
        String[] result = new String[5000];
        for (int i =0;i<strlength;i++)
        {

            parts = abc[i].split(" ");

            if((Integer.parseInt(parts[0])+Integer.parseInt(parts[1]) > Integer.parseInt(parts[2])) && (Integer.parseInt(parts[1])+Integer.parseInt(parts[2]) > Integer.parseInt(parts[0])) && (Integer.parseInt(parts[0])+Integer.parseInt(parts[2]) > Integer.parseInt(parts[1])))
            {
                if(((Integer.parseInt(parts[0])) == Integer.parseInt(parts[1])) && ((Integer.parseInt(parts[1])) == Integer.parseInt(parts[2])) && ((Integer.parseInt(parts[0])) == Integer.parseInt(parts[2])))
                {
                    result[i] = "Equilateral";
                }
                else if(((Integer.parseInt(parts[0])) == Integer.parseInt(parts[1])) || ((Integer.parseInt(parts[1])) == Integer.parseInt(parts[2])) || ((Integer.parseInt(parts[0])) == Integer.parseInt(parts[2])))
                {
                    result[i] = "Isosceles";
                }
            }
            else
                result[i]="None of These";


        }

        return result;
    }


    public static void maximumCupcakes(String[] trips)
    {

        String[] pts = null;
        int n = 0;
        int m = 0;
        int c = 0;
        int initialcc = 0;
        int extracc = 0;
        int finalsum = 0;
        for(int i =0;i<trips.length;i++)
        {
            pts=trips[i].split(" ");
            n = Integer.parseInt(pts[0]);
            c = Integer.parseInt(pts[1]);
            m = Integer.parseInt(pts[2]);
            initialcc = n/c;
            finalsum+= initialcc;
            //System.out.println(initialcc);
            while(initialcc>=m)
            {
                extracc = initialcc/m;
                initialcc = initialcc%m + extracc;
                finalsum += extracc;
            }
            System.out.println(finalsum);
            finalsum=0;
        }
    }

    static int countPalindrome(String s)
    {
        int n = s.length();
        char s_arr[] = s.toCharArray();
        int countpalins[][] = new int[n][n];
        boolean isPalin[][] = new boolean[n][n];
        int numofPalims = 0;
        numofPalims = n;

        for(int i=0;i<n-1;i++)
        {
            if(s_arr[i]==s_arr[i+1])
            {
                countpalins[i][i+1]=1;
                isPalin[i][i+1]=true;
            }
        }
        for(int i =0;i<n;i++)
        {
            isPalin[i][i] = true;
        }
        for(int space = 2;space<n;space++)
        {
            int limit = n - space;
            for(int i = 0; i< limit;i++)
            {
                int j = i+space;
                if(s_arr[i]==s_arr[j] && isPalin[i+1][j-1])
                {
                    isPalin[i][j] = true;
                }
                if(isPalin[i][j]==true)
                {
                    countpalins[i][j] = countpalins[i][j-1]+countpalins[i+1][j] +1 - countpalins[i+1][j-1];
                }
                else
                {
                    countpalins[i][j] = countpalins[i][j-1]+countpalins[i+1][j]- countpalins[i+1][j-1];
                }
            }
        }
        return countpalins[0][n-1] + numofPalims;
    }

}

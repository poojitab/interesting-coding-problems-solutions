package com.poojita;

import org.apache.commons.lang3.tuple.MutablePair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import static java.lang.System.exit;

//Poojita Badarinarayan
/*Program: Main.java
Purpose: To count the number of paths from source to destination with blocks & jumps co-ordinates
Primary Logic Adopted: Backtracking
Input through console: Number of Rows & Columns in a grid, Number of blocks, Block co-ordinates(if any), Number of jumps,  Block co-ordinates(if any)
Functions used: count(),constructMatrix(), main()
Output: The number of paths from source to destination for a given scenario in a nxm grid
*/
public class Main {


    //Function: count - Used to obtain the number of paths from source to destination in a grid
    public static int count(int [][] arrA, int row, int col, int destRow, int destCol, HashMap<MutablePair<Integer,Integer>,MutablePair<Integer,Integer>> jumpsMap){
        //base case
        //check if last row OR column is reached since after that only one path
        
        if(row==destRow-1 && col==destCol-1)
        {
            return 1;
        }
        MutablePair<Integer, Integer> jumpPair = null;
        int left =0;
        int down = 0;
        if(row!=destRow-1 && arrA[row+1][col]!=-1)
        {
             jumpPair = jumpsMap.get(new MutablePair<>(row+1, col));

            if (jumpPair != null)
            {
                left = count(arrA, jumpPair.getLeft().intValue(), jumpPair.getRight().intValue(), destRow, destCol, jumpsMap);
            }
            else
            {
                left = count(arrA, row+1, col, destRow, destCol, jumpsMap);
            }
        }
        if(col!=destCol-1 && arrA[row][col+1]!=-1)
        {
             jumpPair = jumpsMap.get(new MutablePair<>(row, col+1));

            if (jumpPair != null)
            {
                down = count(arrA, jumpPair.getLeft().intValue(), jumpPair.getRight().intValue(), destRow, destCol, jumpsMap);
            }
            else
            {
                down = count(arrA, row,col+1, destRow, destCol, jumpsMap);
            }
        }
        return left + down;
    }

    //Function constructMatrix: Used to construct a matrix with 1s & -1s where -1 being a blocked cell & 1 being an unblocked cell
    public static int[][] constructMatrix (HashSet<MutablePair<Integer,Integer>> blocks, int row, int col)
    {
        int [][]matrix = null;
        if (row > 0 && col > 0)
        {
            matrix = new int[row][col];
            for(int i=0;i<row;i++)
            {
                for(int j=0;j<col;j++)
                {
                    if (blocks.contains(new MutablePair(i,j)))
                    {
                        matrix[i][j] = -1;
                    }
                    else
                    {
                        matrix[i][j] = 1;
                    }
                }
            }

        }
       return matrix;
    }


    public static void main(String[] args)  {

        Scanner scr = new Scanner(System.in);

        int blocks = 0, jumps = 0;
        int rows = 0, cols = 0;
        int countOfPaths = 0;

        HashMap<MutablePair<Integer,Integer>, MutablePair<Integer,Integer>> mapofjumps = new HashMap<MutablePair<Integer,Integer>, MutablePair<Integer,Integer>>();
        HashSet<MutablePair<Integer,Integer>> setofblocks = new HashSet<MutablePair<Integer, Integer>>();


        try
        {
            //Collecting # rows & # columns from user through console
            System.out.println(" Enter the values of rows & columns in a matrix \n ");
            String coordinate = scr.nextLine();
            String[] parts = coordinate.split("\\s+");

            rows = Integer.parseInt(parts[0].trim());
            cols = Integer.parseInt(parts[1].trim());

            //Collecting # blocks from user through console
            System.out.println("Enter the number of blocks, 0 if none \n");
            String num_blocks = scr.nextLine();


            blocks = Integer.parseInt(num_blocks);


            //Adding the block co-ordinates to as a mutable pair to a hashset
            if(blocks >= 0) {
                if (blocks > 0) {
                    System.out.println("Enter the co-ordinates of blocks");

                    for (int i = 0; i < blocks; i++) {
                        String pair_ip = scr.nextLine();
                        String[] pair_ip_parts = pair_ip.split("\\s+");
                        int x = Integer.parseInt(pair_ip_parts[0]);
                        int y = Integer.parseInt(pair_ip_parts[1]);
                        if (x <= rows - 1 && y <= cols - 1) {
                            setofblocks.add(new MutablePair<>(x, y));
                        }
                    }
                }
            }
            else
            {
                System.out.println("Invalid Input for number of blocks! Please try again");
                exit(0);
            }
            //Collecting # jumps from user through console
            System.out.println("Enter the number of jumps, 0 if none \n");
            String num_jumps = scr.nextLine();


            jumps = Integer.parseInt(num_jumps);

            //Adding the jump co-ordinates as mutable pairs; Jump from co-ordinate as key, jump to co-ordinate as value
            if(jumps>=0) {
                if (jumps > 0) {
                    System.out.println("Enter the co-ordinates of jumps: ");

                    for (int i = 0; i < jumps; i++) {
                        String pair_ip = scr.nextLine();
                        String[] pair_ip_parts = pair_ip.split("\\s+");

                        int x1 = Integer.parseInt(pair_ip_parts[0]);
                        int y1 = Integer.parseInt(pair_ip_parts[1]);
                        int x2 = Integer.parseInt(pair_ip_parts[2]);
                        int y2 = Integer.parseInt(pair_ip_parts[3]);

                        if (x1 <= rows - 1 && y1 <= cols - 1 && x2 <= rows - 1 && y2 <= cols - 1) {
                            mapofjumps.put(new MutablePair(x1, y1), new MutablePair(x2, y2));
                        } else {
                            System.out.println("Wrong co-ordinates for jumps entered! Only whole numbers allowed");
                        }
                    }
                }
            }
            else
            {
                System.out.println("Invalid input for number of jumps provided. Please try again");
                exit(0);
            }

            int [][]matrix_returned = constructMatrix(setofblocks, rows, cols);

            countOfPaths = count(matrix_returned,0, 0, rows, cols, mapofjumps);
            System.out.println("The value of paths is: " + countOfPaths);
        }
        catch (NumberFormatException ex)
        {
            System.out.println("Invalid number. Please use whole numbers only !");

        }



    }
}

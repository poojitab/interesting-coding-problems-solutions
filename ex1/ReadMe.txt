Poojita Badarinarayan
Program - Main.java 
IDE Used - IntelliJ Community Edition
JDK version - 1.8

This program takes input from the user for the following items:

Rows & Columns in a grid - Kindly provide these values separated by whitespace/s. Eg: 2 2
Number of blocks in the grid - An integer value >=0
Block Co-ordinates - Kindly provide these values separated by whitespace/s.Eg: 1 1
Number of jumps in the grid - An integer value >=0
Jumps Co-ordinates - Kindly provide the jump from & to values separated by whitespace/s.Eg: 1 1 0 1. Here (1,1) represents jump from co-ordinates & (0,1) represents jump to co-ordinates

I have also made use of a apache library to make use of MutablePair class. A single click of run button shall do the magic!


package com.poojita;

public class Main  {

    public static void main(String[] args) {
	// write your code here

        Worker w1 = new Worker();
        w1.start();

        Worker w2 = new Worker();
        //w2.start();

        Thread t1 = new Thread(new RunnableWorker());
        t1.start();
        Thread t2 = new Thread(new RunnableWorker());
        //t2.start();

        //w1.interrupt();
    }
}

package com.poojita;

/**
 * Created by pooji on 10/15/2017.
 */
public class Worker extends Thread {

    @Override
    public synchronized void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i + "looping");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException E) {
                break;
            }
        }
    }
}
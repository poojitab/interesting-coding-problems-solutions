package com.poojita;

public class Main {

    public static void main(String[] args) {
	// max product with 3 numbers in an integer array
        Main m = new Main();
        int arr[]={1,2,3};
        int res = m.prodarr(arr);
        System.out.print(res);
    }

    public int prodarr(int arr[])
    {
        int max1=Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;

        for(int i =0;i<arr.length;i++)
        {
            if(arr[i] > max1)
            {
                max3=max2;
                max2=max1;
                max1 = arr[i];

            }
            else if(arr[i] > max2 && arr[i] != max1)
            {
                max3=max2;
                max2 = arr[i];
            }
            else if(arr[i] > max3 && arr[i]!= max2 && arr[i]!=max1)
            {
                max3 = arr[i];
            }

        }

        System.out.println(max1+" "+max2+" "+max3);

        return (max1*max2*max3);
    }
}

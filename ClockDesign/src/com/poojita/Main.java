package com.poojita;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class Main {


    public static void main(String[] args) {

        solution("23:12");

    }

    static HashSet<String> printAllKLength(char set[], int k) {
        int n = set.length;
        ArrayList<String> a = new ArrayList<String>();
        HashSet<String> h = new HashSet<String>();
        printAllKLengthRec(set, "", n, k,a,h);
        return h;
    }

    // The main recursive method to print all possible strings of length k
    static void printAllKLengthRec(char set[], String prefix, int n, int k, ArrayList<String> a, HashSet<String> h) {

        // Base case: k is 0, print prefix
        if (k == 0) {
            a.add(prefix);
            if(!h.contains(prefix))
            {
                h.add(prefix);
            }

            return;
        }

        // One by one add all characters from set and recursively
        // call for k equals to k-1
        for (int i = 0; i < n; ++i) {

            // Next character of input added
            String newPrefix = prefix + set[i];

            // k is decreased, because we have added a new character
            printAllKLengthRec(set, newPrefix, n, k - 1,a,h);
        }
    }

    public static String solution(String S) {
        // write your code in Java SE 8
        char[] c = S.replace(":"," ").replace(" ","").toCharArray();
        HashSet<String> result = printAllKLength(c,4);
        List<String> sortedlist = new ArrayList<String>(result);
        //Collections.sort(sortedlist);

        String min_so_far = null;
        String comp2 = S.replace(":"," ").replace(" ", "");
        String xyz = S.replace(":","");
        xyz = xyz.substring(0,2) + ":" + xyz.substring(2, xyz.length());
        min_so_far = xyz;
        ArrayList<Date> f = new ArrayList<Date>();
        if(result!=null)
        {


            SimpleDateFormat ft = new SimpleDateFormat("HH:mm");
            ft.setLenient(false);
            Date mydate = null;
            try
            {
                mydate = ft.parse(S);
            }
            catch(Exception E)
            {

            }

            for(int i =0;i<sortedlist.size();i++)
            {
                try
                {
                    String s = sortedlist.get(i);
                    String s1 = s.replace(":", "");
                    String s2 = s1.substring(0,2) + ":" + s1.substring(2, s1.length());
                    Date d = ft.parse(s2);
                    String comp1 = s1.replace(" ", "");

                    Date d2 = ft.parse(min_so_far);
                    //Date d2 = null;
                    if(comp1.compareTo(comp2)<0)
                    {
                        d.setDate(d.getDate()+1);


                    }

                    if(d.after(mydate))
                    {
                      f.add(d);
                    }

                }

                catch(Exception E)
                {
                    //Do nothing
                    System.out.println("Wrong Time");
                }


            }
            Collections.sort(f);
            String part[] = String.valueOf(f.get(0)).split(" ");
            String part2 = part[3].substring(0,5);
            System.out.println(part2);



        }



        return min_so_far;
    }



    public static ArrayList<String> permutation(String str) {
        ArrayList<String> arrList = new ArrayList<String>();

        String newstr = str.replace(":", "");
        permutation("", str, arrList);



        return arrList;
    }

    private static void permutation(String prefix, String str, ArrayList<String> list) {
        int n = str.length();
        if (n == 0)
        {
            list.add(prefix);
        }
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n), list);
        }
    }



}

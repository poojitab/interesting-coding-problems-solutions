package com.poojita;

/*Log aggregation:

input is stream like:

1.2.3.4 /some/path.html
1.1.2.3 /some/other/path.txt
1.2.3.4 /some/other/path.txt

containing IP address and the path with no ordering among the rows.

The output we want is:

1.2.3.4 2 /some/path.html /some/other/path.txt
1.1.2.3 1 /some/other/path.txt

containing the IP address, the count for that IP, and the list of unique paths seen by that IP.
*/

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Main m = new Main();
        String input = "1.2.3.4 /some/path.html\n" +
            "1.1.2.3 /some/other/path.txt\n" +
            "1.2.3.4 /some/other/path.txt";
        m.func1(input);
    }

    public void func1(String ip)
    {
        String parts[] = ip.split("\n");
        HashMap<String, HashMap<String, Integer>> hm = new HashMap<>();
        for(int i =0;i<parts.length;i++)
        {
            String parts2[] = parts[i].split(" ");
            if(hm.containsKey(parts2[0]))
            {
                HashMap<String, Integer> op = hm.get(parts2[0]);
                if(op.containsKey(parts[1]))
                {
                    op.put(parts2[1],op.get(parts2[1])+1);

                }
                else
                    op.put(parts2[1],1);
                    hm.put(parts2[0],op);

            }

            else
            {
                HashMap<String, Integer> op1 = new HashMap<String, Integer>();
                op1.put(parts2[1],1);
                hm.put(parts2[0],op1);
            }

            //System.out.println(parts2[0]);
        }

        Iterator it = hm.entrySet().iterator();
        while(it.hasNext())
        {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            String mykey = pair.getKey().toString();


        }
        int sum =0;
        for(String key: hm.keySet())
        {
            System.out.print(key+" ");
            HashMap<String, Integer> res = hm.get(key);

            for(Integer i: res.values())
            {
                sum += i;
            }
            System.out.print(sum+" ");
            for(String key1: res.keySet())
            {
                System.out.print(key1+ " ");
            }
            System.out.println("");
        }


    }


}
